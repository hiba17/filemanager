package logging;

import java.io.*;
import java.time.LocalDate;

public class Logging {
    //I have already created the file to logg in before hand
    //Using buffer-writer to open the file and start writing in it
    private final BufferedWriter writer = new BufferedWriter(new FileWriter("src/main/java/logging/Loggfile.txt"));

    public Logging() throws IOException {
        String str = "------- LOGGED IN: -------\n";
        writer.write(str);


    }
    //Close the writer buffer-writer before printing out the file in terminal
    public void close() throws IOException{
        writer.close();
        String line;
        try (BufferedReader reader = new BufferedReader(new FileReader("src/main/java/logging/Loggfile.txt"))) {
            line =reader.readLine();
            while (line != null) {
                System.out.println(line);
                line =reader.readLine();
            }
            System.out.println("\n------------------------------------------------------\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    //this method is used to add lines to the loggfile adding the date in the front of the line
    public void writeToFile(String output) throws IOException{
        writer.append(LocalDate.now()+" "+ output+"\n\n");
    }
}
