import filservice.PickFiles;
import logging.Logging;
import java.io.File;
import java.util.Scanner;

public class Main {
    //Message that will be repeated every time user comes back to main
    static void startMessage(){
        System.out.println("""
                Do you want to:
                 - start fileManipulation (Write s)
                  - quit & see what we have logged (Write q)""");

    }
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String path = "src/main/resources/Files";
        try{
            // Getting resource(Folder) from class loader
            File folder = new File(path);
            //create file to write in as we go
            Logging logg = new Logging();
            //Start message
            System.out.println("__WELCOME TO THE FILE SYSTEM MANAGER__"+"\n");
            startMessage();
            String sq = input.next();

            //as long as user hasn't written q
            while(!sq.equals("q")){
                //If user pressed manipulation we create an object of pick files,
                //and send inn folder,the scanner object the path, and the logg object
                if(sq.equals("s")) new PickFiles(folder, input, path, logg);
                // If it is not q or s, the user typed in something wrong, they try again
                else{
                    System.out.println( "You wrote something wrong!\n " );

                }
                startMessage();
                sq = input.next();
            }
            //if q then we close the open bufferedwriter object, and we print out logg info
            logg.close();
        }catch(Exception e){
            System.out.println(" You got an Exception mistake: "+e.getMessage());
        }

        //end of management system
        System.out.println("\n Thanks! See you soon :) ");
    }

}