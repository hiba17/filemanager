package filservice;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;
import logging.Logging;
import org.apache.commons.lang3.StringUtils;

public class DraculaManipulation {

    private final String path;

    public DraculaManipulation(String path, Scanner input, Logging logg)throws IOException {
        System.out.println("__WELCOME TO THE DRACULA-MANIPULATION__"+"\n");
        //The first thing that happen is a presentation of file-title,size and number of lines
        this.path =path;
        //Find duration of time used by function presentinfo()
        long startTime = System.currentTimeMillis();
        String output = presentInfo();
        long stopTime = System.currentTimeMillis();
        long presentInfoStamp = stopTime - startTime;
        //logg the timedifference
        logg.writeToFile("Method presentInfo gives:\n" + output + "- This function took " + presentInfoStamp + " ms to execute.");

        //giving user possibility to look for a word in the text
        System.out.println("Do you have a word you want to search for in the file? (y/n)" +"\n");
        String next = input.next();
        //If the answer is yes(not case sensitive)
        while(next.equalsIgnoreCase("y")){
            System.out.println("Write the word:" +"\n");
            next = input.next();
            //Find duration of time used by function search()
            long starttime = System.currentTimeMillis();
            String op = search(next);
            long stoptime = System.currentTimeMillis();
            long searchStamp = stoptime - starttime;
            logg.writeToFile("Method search gives:\n" + op + "- This function took " + searchStamp + " ms to execute.\n");
            //find out if user wants to search for more words if so the loop repeats itself
            System.out.println("Do you have a another word you want to search for in the file? (y/n)" +"\n");
            next = input.next();
        }

    }
    //
    public String search(String input){

        int count = 0;
        boolean exists=false;
        String output="";
        //uses bufferreader to read thou dracula file and find input(given word from user)
        String line;
        try (BufferedReader reader = new BufferedReader(new FileReader(path))) {
            line =reader.readLine();
            while ( line != null) {
                //finds out if the line we are reading from file contains the word given by user
                //turned the word into lowercase so it stays case-insensitive
                if(line.contains(input.toLowerCase())){
                    //countmatches finds number of words in a given line
                    count = count+ StringUtils.countMatches(line, input.toLowerCase());
                    exists=true;
                }
                //reads new line
                line =reader.readLine();
            }
            //if exists ==true then print out count
            if(exists) {
                output="The word "+ input + " exists, and is counted for "+count+" times in the file. \n";

            }else{
                output = "The word "+ input + " unfortunately doesn't exist.\n";

            }
            System.out.println(output);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return output;
    }

    public String presentInfo(){
        //printing filename
        String info="Name of the File: Dracula.txt\n";
        System.out.println(info);

        //finding size of file, with a try catch
        Path p = Paths.get(path);
        try {
            long bytes = Files.size(p);
            String size= "Size of the file is: "+bytes+" bytes\n";
            info=info+size;
            System.out.println(size);
        } catch (IOException e) {
            e.printStackTrace();
        }

        //find number of file-lines
        long lines = 0;
        try (BufferedReader reader = new BufferedReader(new FileReader(path))) {
            while (reader.readLine() != null) lines++;
            String numberofLines="Number of lines in the file: "+lines+" lines\n";
            info += numberofLines;
            System.out.println(numberofLines);

        } catch (IOException e) {
            e.printStackTrace();
        }
        return info;

    }
}
