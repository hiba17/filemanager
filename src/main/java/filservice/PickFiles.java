package filservice;

import logging.Logging;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.Scanner;
import java.util.regex.Pattern;

public class PickFiles {

    // Message that will be repeated every time user comes back to main
    public void startMessage(){
        System.out.println("""


                Do you want to:\s
                - be presented with all the filenames(Press n)
                - Pick a file based on the extensions (Press e)
                - Manipulate the dracula text ( Press d)\s
                - Go back to start (Press s)\s
                """);
    }
    // method can throw an exception, therefore throws IOException
    public PickFiles(File folder, Scanner input,String path, Logging logg)throws IOException {
        //creates a list of all the lists in th folder
        File[] listOfFiles = folder.listFiles();
        startMessage();
        String next = input.next();
        //as long as they don't want to back to start
        while (!next.matches("s")) {
            //if the user wants to see all the filenames
            if (next.matches("n")) {
                //we get time before and after function execution in ms
                long startTime = System.currentTimeMillis();
                assert null != listOfFiles;
                String output = presentFilenames(listOfFiles);
                long stopTime = System.currentTimeMillis();
                //calculate duration of the function execution
                //duration of a function in milliseconds
                long pFilenamesStamp = stopTime - startTime;
                //sending the string to get logged in logg-file.txt
                logg.writeToFile("Method presentfilenames gives:\n" + output + "\n- This function took " + pFilenamesStamp + " ms to execute.");

            }
            //if the user wants to see file based on the extensions
            else if (next.matches("e")) {

                long startTime = System.currentTimeMillis();

                assert listOfFiles != null;
                String output1 = presentExtensions(listOfFiles) + "\n";
                //get input from user about wished extension
                System.out.println("Write the extension you want to look through:  ");
                String extension = input.next();
                String output2 = presentFilesFromExtension(folder, extension)+"\n";

                long stopTime = System.currentTimeMillis();
                long pExtensionStamp = stopTime - startTime;
                logg.writeToFile("Methods presentExtension gives:\n" + output1 +
                                 " Method presentFilesFromExtension with " + extension + " as extension gives:\n" +
                                 output2 + "- These functions" +
                                 " took " + pExtensionStamp + " ms to execute.");
            }
            //if user wants to manipulate the dracula file create a dracula object and send in
            // a new path with added dracula.txt and scanner and the logg object
            else if (next.matches("d")) {
                new DraculaManipulation(path+"/Dracula.txt",input,logg);
            }
            //if the input is none of the above , the user made a mistake
            else {
                System.out.println( "You wrote something wrong! try again \n " );

            }
            //get the start-message again to see what user wants to do
            startMessage();
            next = input.next();
        }

    }
    //print and logg inn al the filenames in the resource folder
    public String presentFilenames(File[] listOfFiles){
        System.out.println("The Filenames are:  ");
        String logg="";
        for (File listOfFile : listOfFiles) {
            logg += listOfFile.getName() + ", ";
            System.out.print(listOfFile.getName() + ", \n");
        }
        return logg;
    }
    // Print out all the different extension
    public String presentExtensions(File[] listOfFiles){
        System.out.println("The FileExtensions are:  ");
        String extension="";
        //loop through list of Files
        for (File listOfFile : listOfFiles) {
            //split the filename so we only get the extension part
            String[] numbers = listOfFile.getName().split(Pattern.quote("."));
            //if it matches with the input form the user print it out, add it to logg-string
            if (!extension.contains(numbers[1])) {
                System.out.print("." + numbers[1] + " , ");
                extension = "." + extension + numbers[1] + " , ";
            }
        }
        return extension;

    }
    //prints filenames only with a given extension
    public String presentFilesFromExtension( File f, String extension){
        String logg="";
        System.out.println("These are the files with the extension "+ extension+":");
        // This filter will only include files ending with extension chosen
        FilenameFilter filter = new FilenameFilter() {
            @Override
            public boolean accept(File f, String name) {
                return name.endsWith(extension);
            }
        };

        // This is how to apply the filter
        //loop through the sorted files ,printing out their names
        String[] files = f.list(filter);
        for (int i = 0; i <files.length ; i++) {
            logg=logg+files[i]+ ", ";
            System.out.print(files[i]+ ", ");
        }
        return logg;
    }

}
