# System-Manager

## General Info
This is a system file manager that give user the opportunity to 
- Print out filenames from the resource folder
- Pick files by extensions 
- Manipulate dracula.txt by searching for words and see if they are used

## Technologies
- IntelliJ
- Java SE
- Gradle

## Prerequesites
Install java using this link:
````
https://www.oracle.com/java/technologies/javase-downloads.html
````
Install IntelliJ code using this link:
````
https://www.jetbrains.com/idea/download/#section=windows
````
Implement json libaray by going to build.gradle file and typing this under dependencies:
````
    implementation 'org.json:json:20171018'
````
## Features
1. Takes input form user to direct the user to where they want to go

2. search engine for dracula file

3. Prints out filenames in terminal

3. prints out Extensions as well as files from a given extension picked by user

4. It prints out title , size and number of lines in dracula.txt

5. Logg every function executed and its date and timeduration for each function and writes it to loggfile.txt

6. Allowes user to quit the programs and print out the loggfile

## Difficulties

I had difficulties knowing how to divide the program into its appropriate classes and how much to have per class.
I also struggled with how I was going to fit the logging critera of the task to my program.I didn't plan well ahead
for that, which in hindsight was something I should have done. I was going to crate a testclass to test everything, but 
did not get the chance to unfortunately.The system has problems with .jar file. It is created, but i don't know why it doesn't want to execute.
